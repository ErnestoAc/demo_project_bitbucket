
package miprimeraaplicacionjava1;


public class MiPrimeraAplicacionJava1 {

    public static void main(String[] args) {
        
        //declarar algunos tipos de datos
        String nombreCompleto, direccion, estadoCivil;
        int edad;
        double gastosDiarios;
        char genero;
        boolean poseeVehiculo;
        gastosDiarios = 34.45;
        
        //inicializacion de variables
        nombreCompleto = "Camila Esperanza Rosales";
        direccion = "Calle La Amargura Desvío La Tristeza";
        estadoCivil = "Soltera";
        edad = 30;
        genero = 'F';
        poseeVehiculo = false;
        
        System.out.println("Nombre Completo: " + nombreCompleto); 
        System.out.println("Dirección: " + direccion);
        System.out.println("Edad: "+ edad );
        System.out.println("Gastos Diarios: " + gastosDiarios);
        System.out.println("Genero: "+ genero);
        System.out.println("Posee Vehiculo: " + poseeVehiculo);
        
    }
    
}
