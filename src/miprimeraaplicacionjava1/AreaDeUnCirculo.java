
package miprimeraaplicacionjava1;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class AreaDeUnCirculo {
    public static void main(String[] args) {
        NumberFormat formato = new DecimalFormat("#0.00");
        
        //declara variables
        double area, radio;
       double pi=3.1416; //la palabra final idica que el vañor nunc cambiará
        
        //inicialización de variables
        area = 145;
        
        //uso de variables
        radio = Math.sqrt(area/pi);
        
        System.out.println("Radio: " + formato.format(radio) + " cm");
        
    }
}
