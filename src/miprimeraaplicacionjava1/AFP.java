
package miprimeraaplicacionjava1;
import java.util.Scanner;

public class AFP {
    public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);
        
        
        double salario, afpT, afpE ,pAfpT, pAfpE;
        
        System.out.println("Digite un salario");
        salario = leer.nextDouble();
        
        afpT = salario * 0.060;
        afpE = salario * 0.065;
        
        pAfpT = salario - afpT;
        pAfpE = salario - afpE;
        
        System.out.println("El descuento de AFP Trabajador es: $"+ afpT);
        System.out.println("El Salario Trabajador con descuentos es: $: " + pAfpT +"\n \n" );
        
        System.out.println("El descuento de AFP Empleador es: $"+ afpE);
        System.out.println("El Salario Empleador con descuentos es: $" + pAfpE );
        
        double AFP =  afpT + afpE;
        System.out.println("TOTAL AFP = $" +AFP +"\n");
        
        
    }
   
}
