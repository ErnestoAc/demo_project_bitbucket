
package miprimeraaplicacionjava1;


public class CalculoInteresSimple {
    public static void main(String[] args) {
        //declara Variable
        double interes, capitalPrestado, tiempo,tasadeInteres,tiempoEnMeses;
        
       //inicializar variables
        interes=144;
        capitalPrestado = 1200.00;
        tasadeInteres = 0.08;
        
        //uso de variables
        tiempo = interes / (capitalPrestado * tasadeInteres);
        tiempoEnMeses = tiempo *12;
        
        System.out.println("Tiempo: " + tiempo + " años");
         System.out.println("Tiempo: " + tiempoEnMeses + " meses");
    }
}
